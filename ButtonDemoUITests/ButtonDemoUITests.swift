//
//  ButtonDemoUITests.swift
//  ButtonDemo
//
//  Created by Mastercard on 11/06/20.
//  Copyright © 2020 Mastercard. All rights reserved.
//
		

import XCTest

class ButtonDemoUITests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.

        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }


    func testButtonCheck() {
        
        let app = XCUIApplication()
        app.launch()
        
        app.buttons["Print on Label"].tap()
        XCTAssertNotNil(app.staticTexts["Hello..!! Welcome Back"])
    }
    
}
