//
//  ButtonDemoTests.swift
//  ButtonDemo
//
//  Created by Mastercard on 11/06/20.
//  Copyright © 2020 Mastercard. All rights reserved.
//
		

import XCTest

class ButtonDemoTests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testAppName() {
       let appName = AppName()
        let result = appName.getAppName() == "Button Demo"
        XCTAssertTrue(result, "Name should match")
    }
    
    func testAppNameNotNull() {
        let appName = AppName()
        XCTAssertNotNil(appName,"should not be null")
    }

}
